package com.sanggil.studycafemanager.repository;

import com.sanggil.studycafemanager.entity.StudyCafeCustomer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudyCafeCustomerRepository extends JpaRepository<StudyCafeCustomer, Long> {
}
