package com.sanggil.studycafemanager.service;

import com.sanggil.studycafemanager.entity.StudyCafeCustomer;
import com.sanggil.studycafemanager.model.CustomerItem;
import com.sanggil.studycafemanager.model.CustomerRequest;
import com.sanggil.studycafemanager.model.CustomerTimeUpdateRequest;
import com.sanggil.studycafemanager.repository.StudyCafeCustomerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class StudyCafeCustomerService {
    private final StudyCafeCustomerRepository studyCafeCustomerRepository;


    public void setCustomer(CustomerRequest request) {
        StudyCafeCustomer addData = new StudyCafeCustomer();
        addData.setCustomerName(request.getCustomerName());
        addData.setCustomerPhone(request.getCustomerPhone());
        addData.setBirthday(request.getBirthday());
        addData.setUseTime(request.getUseTime());
        addData.setTimeStart(LocalDateTime.now());
        addData.setTimeEnd(LocalDateTime.now().plusHours(request.getUseTime()));

        studyCafeCustomerRepository.save(addData);
    }

    public List<CustomerItem> getCustomer() {
        List<StudyCafeCustomer> originList = studyCafeCustomerRepository.findAll();

        List<CustomerItem> result = new LinkedList<>();

        for (StudyCafeCustomer item : originList) {
            CustomerItem addItem = new CustomerItem();
            addItem.setId(item.getId());
            addItem.setCustomerInfo(item.getCustomerName() + "/" + item.getCustomerPhone());
            addItem.setBirthday(item.getBirthday());
            addItem.setUseTime(item.getUseTime());
            addItem.setUseTimeFullText(item.getTimeStart() + "~" + item.getTimeEnd());
            addItem.setIsOut(item.getTimeOut() == null ? false : true);
            addItem.setTimeOut(item.getTimeOut());

            result.add(addItem);
        }

        return result;
    }

    public void putCustomerTime(long id, CustomerTimeUpdateRequest request) {
        StudyCafeCustomer originData = studyCafeCustomerRepository.findById(id).orElseThrow();
        originData.setUseTime(request.getUseTime());
        originData.setTimeEnd(originData.getTimeStart().plusHours(request.getUseTime()));

        studyCafeCustomerRepository.save(originData);
    }

    public void putCustomerTimeOut(long id) {
        StudyCafeCustomer originData = studyCafeCustomerRepository.findById(id).orElseThrow();
        originData.setTimeOut(LocalDateTime.now());

        studyCafeCustomerRepository.save(originData);
    }

    public void delCustomer(long id) {
        studyCafeCustomerRepository.deleteById(id);
    }
}
