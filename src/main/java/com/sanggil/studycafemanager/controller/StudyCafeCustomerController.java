package com.sanggil.studycafemanager.controller;

import com.sanggil.studycafemanager.model.CustomerItem;
import com.sanggil.studycafemanager.model.CustomerRequest;
import com.sanggil.studycafemanager.model.CustomerTimeUpdateRequest;
import com.sanggil.studycafemanager.service.StudyCafeCustomerService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/customer")
public class StudyCafeCustomerController {
    private final StudyCafeCustomerService studyCafeCustomerService;

    @PostMapping("/data")
    public String setCustomer(@RequestBody @Valid CustomerRequest request) {
        studyCafeCustomerService.setCustomer(request);

        return "OK";
    }

    @GetMapping("/all")
    public List<CustomerItem> getCustomer() {
        List<CustomerItem> result = studyCafeCustomerService.getCustomer();

        return result;
    }

    @PutMapping("/time/id/{id}")
    public String putCustomerTime(@PathVariable long id, @RequestBody @Valid CustomerTimeUpdateRequest request) {
        studyCafeCustomerService.putCustomerTime(id, request);

        return "OK";
    }

    @PutMapping("/time-out/id/{id}")
    public String putCustomerTimeOut(@PathVariable long id) {
        studyCafeCustomerService.putCustomerTimeOut(id);

        return "OK";
    }

    @DeleteMapping("/sign-out/id/{id}")
    public String delCustomer(@PathVariable long id) {
        studyCafeCustomerService.delCustomer(id);

        return "OK";
    }
}
