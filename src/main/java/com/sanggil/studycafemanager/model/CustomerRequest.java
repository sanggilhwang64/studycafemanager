package com.sanggil.studycafemanager.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
public class CustomerRequest {
    @NotNull
    @Length(min = 2, max = 20)
    private String customerName;

    @NotNull
    @Length(min = 11, max = 20)
    private String customerPhone;

    @NotNull
    private LocalDate birthday;

    @NotNull
    @Min(value = 1)
    @Max(value = 12)
    private Integer useTime;


}
