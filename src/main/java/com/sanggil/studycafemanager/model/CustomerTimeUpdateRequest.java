package com.sanggil.studycafemanager.model;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Getter
@Setter
public class CustomerTimeUpdateRequest {
    @NotNull
    @Min(value = 1)
    @Max(value = 12)
    private Integer useTime;
}
