package com.sanggil.studycafemanager.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@Setter
public class CustomerItem {
    private Long id;

    private String customerInfo;

    private LocalDate birthday;

    private Integer useTime;

    private String useTimeFullText;

    private Boolean isOut;

    private LocalDateTime timeOut;
}
